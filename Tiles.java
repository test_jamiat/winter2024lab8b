public enum Tiles{
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("H"),
	CASTLE("C");
	
	private String name;
	
	private Tiles (String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}
}
