import java.util.Random;
public class Board{
	private Tiles[][] grid;
	final int SIZE = 5;
	
	public Board(){
		this.grid = new Tiles [SIZE][SIZE];
		for(int i = 0; i<grid.length; i++){
			for(int x = 0; x <grid[i].length; x++){
				this.grid[i][x] = Tiles.BLANK;
			}
			Random rng = new Random();
			int hiddenCol = rng.nextInt(grid[i].length);
			this.grid[i][hiddenCol] = Tiles.HIDDEN_WALL;
		}
	}
	
	public String toString(){
		String arr = "";
		for(Tiles[] x: this.grid){
			arr+="\n";
			for(Tiles y: x){
				arr+=y.getName() + " ";
			}
		}
		return arr;
	}
	
	public int placeToken(int row, int col){
		if(row >= 0 && row <SIZE && col >= 0 && col <SIZE ){
			if((this.grid[row][col] == Tiles.WALL) || (this.grid[row][col] == Tiles.CASTLE)){
				return -1;
			}else if(this.grid[row][col] == Tiles.HIDDEN_WALL){
				return 1;
			} else if(this.grid[row][col] == Tiles.BLANK){
				this.grid[row][col] = Tiles.CASTLE;
				return 0;
			}
			
		}else{
			return -2;
		}
		return -2;
	}
	
	
	
}