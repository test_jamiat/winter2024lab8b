import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Board board = new Board();
		System.out.println(board);

		System.out.println(" Hello dear player!!! \n Welcome to the game of Tic Tac Toe. \n We are very happy to have you here. \n Play a game with our AI to get started ;)");
		int numsCastles = 5;
		int turns = 0;
		while(numsCastles > 0 && turns < 8){
			System.out.println(board);
			System.out.println(numsCastles);
			System.out.println(turns);
			
			Scanner reader = new Scanner(System.in);
			System.out.println("Input the ROW you would like to put your token. [0-4]");
			int row = reader.nextInt();
			System.out.println("Input the COLUMN you would like to put your token. [0-4]");
			int col = reader.nextInt();
			int result = board.placeToken(row, col);
			
			while(result < 0){
				System.out.println("Please re-enter a new row and column");
				System.out.println("AGAIN: Input the ROW you would like to put your token. [0-4]");
				row = reader.nextInt();
				System.out.println("AGAIN: Input the COLUMN you would like to put your token. [0-4]");
				col = reader.nextInt();
				result = board.placeToken(row, col);
			}
			
			if(result == 1){
				System.out.println("BZZZZZZZZZ!! WALL ALERT");
				turns++;
			}else if(result == 0){
				System.out.println("SUPRISINGLY, YOU HAVE SUCCESSFULLY PLACED YOUR CASTLE AT THIS POSITION");
				turns++;
				numsCastles--;
			}
		}
		System.out.println(board);
		
		if(numsCastles == 0){
			System.out.println("YOU ACTUALY WON??? NOW THAT'S SUMN TO BRAG ABOUT");
		} else{
			System.out.println("THIS WAS EXPECTED BUT UNFORTUNATELY, YOU LOST");
		}
	}
}